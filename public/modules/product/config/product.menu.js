'use strict';

// Configuring the Articles module
angular.module('mean').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem({
			id: 'topbar',
			category: 'products',
			title: 'Products',
			type: 'dropdown',
		});
		Menus.addSubMenuItem({
			parentId: 'topbar',
			category: 'products',
			title: 'List Products',
			route: 'product.list'
		});
	}
]);
