'use strict';

// Setting up route
angular.module('product').config(['$stateProvider',
	function($stateProvider) {
		$stateProvider.
			state('product', {
				url: '/product' ,
				abstract: true,
				template: '<div ui-view> </div>',
				controller: 'ProductCtrl'
			}).
			state('product.list', {
				url:'',
				templateUrl: 'modules/product/product.list.html',
				controller:'ProductListCtrl'
			}).
			state('product.show', {
				url: '/:id' ,
				templateUrl: 'modules/product/product.show.html',
				controller:'ProductShowCtrl'
			})
			.state('product.show.newpersona', {
				url: '/persona' ,
				templateUrl: 'modules/product/persona/persona.new.html',
				controller:'PersonaShowCtrl'
			})
			.state('product.show.persona', {
				url: '/persona/:personaID' ,
				templateUrl: 'modules/product/persona/persona.show.html',
				controller:'PersonaShowCtrl'
			});
	}
]);
