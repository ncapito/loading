'use strict';


angular.module('product').controller('PersonaShowCtrl', ['Persona', '$log',
    '$scope', '$state', '$location', 'Authentication',
    function(Persona, $log, $scope, $state, $location, Authentication) {

      $scope.loading = false;
      $scope.save = save;
      $scope.isEditing = isEditing;
      $scope.cancel = cancel;
      $scope.edit = edit;
      $scope.prevent = prevent;
      $scope.validate = validate;
      $scope.saveField = saveField;

      var _edit = {};


      function saveField(eventType, data){
        if(validate(eventType, data) === true){
          save();
        }
      }

      function validate(eventType, data){
        $log.info($scope.persona, data);
        if(angular.isDefined(data) && data){
          return true;
        }
        return eventType + ' is required';
      }

      function prevent($event){
        $event.preventDefault();
        $event.stopPropagation();
      }

      function isEditing(key){
        return _edit[key];
      }

      function cancel(form){
        if(form){
          form.$setPristine();
        }
        _edit = {};
        $scope.persona.$get();
      }
      function edit(key){
        _edit[key] = true;
      }
      function save(form){

        var name = '$update';
        if(!$scope.persona._id){
          name = '$save';
        }

        $scope.persona[name](success, error);

        function success(result){
          $scope.loading = false;
          _edit = {};
          $state.go('product.show.persona', { personaID: $scope.persona._id});
        }
        function error(result){
          $log.error(result);
          $scope.loading = false;
        }
      }

      $log.info('loading Persona ', $state.params.personaID);
      $scope.persona = new Persona({
        product: $state.params.id,
        _id: $state.params.personaID || undefined,
        goals:[],
        frustrations:[],
        behavoirs:[]
      });
      if($scope.persona._id){
        $scope.persona.$get();
      }
		}
  ]);
