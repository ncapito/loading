'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('product').factory('Product', ['$resource',
	function($resource) {
		return $resource('/products/:productID', {
			productID: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]).factory('Persona', ['$resource',
function($resource) {
	return $resource('/personas/:personaID', {
		personaID: '@_id'
	}, {
		update: {
			method: 'PUT'
		}
	});
}
]);
