'use strict';


angular.module('product').directive('contenteditable', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {

      function read() {
        if(ngModel.$viewValue !== element.html()){
          ngModel.$setViewValue(element.html());
        }
      }

      ngModel.$render = function() {
        element.html(ngModel.$viewValue || '');
      };

      element.bind('blur keyup change', function() {
        scope.$apply(read);
      });
    }
  };
});

angular.module('product').controller('ProductCtrl', ['Product',
    '$scope', '$state', '$location', 'Authentication',
    function(Product, $scope, $state, $location, Authentication) {
      	$scope.authentication = Authentication;
		}
  ])
  .controller('ProductListCtrl', ['Product',
    '$scope', '$state', '$location', 'Authentication',
    function(Product, $scope, $state, $location, Authentication) {

      Product.query(
        function(data) {
          $scope.products = data;
        },
        function(error) {
          alert('error');
        }
      );

    }
  ]).controller('ProductShowCtrl', ['Product', '$log',
    '$scope', '$state', '$location', 'Authentication',
    function(Product, $log, $scope, $state, $location, Authentication) {

      $scope.$state = $state;
      $scope.loading = false;
      $scope.save = save;
      $scope.isEditing = isEditing;
      $scope.cancel = cancel;
      $scope.edit = edit;
      $scope.prevent = prevent;
      $scope.validate = validate;
      $scope.saveField = saveField;

      var _edit = {};


      function saveField(eventType, data){
        if(validate(eventType, data) === true){
          save();
        }
      }

      function validate(eventType, data){
        $log.info($scope.product, data);
        if(angular.isDefined(data) && data){
          return true;
        }
        return eventType + ' is required';
      }

      function prevent($event){
        $event.preventDefault();
        $event.stopPropagation();
      }

      function isEditing(key){
        return _edit[key];
      }

      function cancel(form){
        if(form){
          form.$setPristine();
        }
        _edit = {};
        $scope.product.$get();
      }
      function edit(key){
        _edit[key] = true;
      }
      function save(form){

        var name = '$update';
        if(!$scope.product._id){
          name = '$save';
        }

        $scope.product[name](success, error);

        function success(result){
          if(form){
            form.$setPristine();
          }
          $scope.loading = false;
          _edit = {};
          $state.go('product.show', { id: $scope.product._id});
        }
        function error(result){
          $log.error(result);
          $scope.loading = false;
        }
      }

      $log.info('loading product ', $state.params.id);
      $scope.product = new Product({
        _id: $state.params.id || undefined
      });
      if($scope.product._id){
        $scope.product.$get();
      }
		}
  ]);
