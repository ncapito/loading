'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('espn').factory('Espn', ['Keys','$q', '$http',
  function(Keys, $q, $http) {
    var coreData  ={
      apikey: Keys.ESPN,
      JSON_CALLBACK: ''
    };


    function handleError( response ) {
        // The API response from the server should be returned in a
        // nomralized format. However, if the request was not handled by the
        // server (or what not handles properly - ex. server error), then we
        // may have to normalize it on our end, as best we can.
        if (
            ! angular.isObject( response.data ) ||
            ! response.data.message
            ) {

            return( $q.reject( 'An unknown error occurred.') );

        }

        // Otherwise, use expected error message.
        return( $q.reject( response.data.message ) );

    }
    // I transform the successful response, unwrapping the application data
    // from the API response payload.
    function handleSuccess( response ) {
        return( response.data );

    }

    return {
      top: function(data) {
        var request = $http.get(
             'http://api.espn.com/v1/now/top',
            {params: angular.extend({}, coreData, data)}
        );
        return( request.then( handleSuccess, handleError ) );
      },
      now: function(data) {
        var request = $http.get(
             'http://api.espn.com/v1/now',
            {params: angular.extend({}, coreData, data)}
        );
        return( request.then( handleSuccess, handleError ) );
      },
      popular: function(data) {
        var request = $http.get(
             'http://api.espn.com/v1/now/popular',
            {params: angular.extend({}, coreData, data)}
        );
        return( request.then( handleSuccess, handleError ) );
      }
    };
  }
]);
