'use strict';


angular.module('admin').controller('ProjectListCtrl',[function(){}]);
angular.module('admin').controller('ProjectShowCtrl',[function(){}]);
angular.module('admin').controller('ProjectCreateCtrl',[function(){}]);
angular.module('admin').controller('ProjectEditCtrl',[function(){}]);

angular.module('admin')
	.controller('ProjectCtrl', ['$scope', '$stateParams', '$state', '$location', 'Authentication', 'Project',
	function($scope, $stateParams, $state, $location, Authentication, Project) {
		$scope.authentication = Authentication;
		$scope.create = function() {
			var project = new Project({
				title: this.title,
				content: this.content
			});
			project.$save(function(response) {
				$state.go('admin.project._.view', {projectId: response._id});
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

			this.title = '';
			this.content = '';
		};

		$scope.remove = function(project) {
			if (project) {
				project.$remove();

				for (var i in $scope.projects) {
					if ($scope.projects[i] === project) {
						$scope.projects.splice(i, 1);
					}
				}
			} else {
				$scope.project.$remove(function() {
						$state.go('admin.project.list');
				});
			}
		};

		$scope.update = function() {
			var project = $scope.project;

			project.$update(function(response) {
				$state.go('admin.project._.view', {projectId: response._id});
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.projects = Project.query();
		};

		$scope.findOne = function() {
			$scope.project = Project.get({
				projectId: $stateParams.projectId
			});
		};
	}
]);
