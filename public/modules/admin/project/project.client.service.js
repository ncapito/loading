'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('admin').factory('Project', ['$resource',
	function($resource) {
		return $resource('admin/project/:projectId', {
			peopleId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
