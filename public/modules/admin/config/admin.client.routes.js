'use strict';

// Setting up route
angular.module('mean').config(['$stateProvider',
	function($stateProvider) {
		// Peoples state routing
		function scaffold(resource){
			var lowerResource = resource.toLowerCase(),
					capitalizedResource = resource.capitalize(),
					firstLetterLower = resource.uncapitalize();
			$stateProvider.
				state('admin.' + lowerResource, {
					abstract: true,
					url: '/' + lowerResource + '/' ,
					template: '<div ui-view> </div>',
					controller: capitalizedResource + 'Ctrl'
				}).
			state('admin.' + lowerResource + '.list', {
				url: '',
				templateUrl: 'modules/admin/' + lowerResource +'/views/list.html',
				controller: capitalizedResource+ 'ListCtrl'
			}).
			state('admin.' + lowerResource + '.create', {
				url: 'create',
				templateUrl: 'modules/admin/' + lowerResource +'/views/create.html',
				controller: capitalizedResource+ 'CreateCtrl'
			}).
			state('admin.' + lowerResource + '._', {
				url: ':'+ firstLetterLower+ 'Id',
				abstract: true,
				template: '<div ui-view> </div>',
				controller: capitalizedResource+ 'ShowCtrl'
			}).
			state('admin.' + lowerResource + '._.view', {
				url: '',
				templateUrl: 'modules/admin/' + lowerResource +'/views/show.html',
			}).
			state('admin.' + lowerResource + '._.edit', {
				url: '/edit',
				templateUrl: 'modules/admin/' + resource +'/views/edit.html',
				controller: capitalizedResource+ 'EditCtrl'
			});
		}
		$stateProvider.
			state('admin',{
					url: '/admin',
					abstract: true,
					template: '<div ui-view> </div>'
			});

		scaffold('people');
		scaffold('project');
		scaffold('gameMeta');
	}
]);
