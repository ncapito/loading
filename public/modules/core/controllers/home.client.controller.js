'use strict';


angular.module('core').factory('LoadingService', ['$q', function($q){
	return function(){
		var loadingFunctions = [];
		var loadingQs = [];

		this.waitFor = function(func){
			 loadingFunctions.push(func);
		};

		this.initialize = function(success, fail){
			angular.forEach(loadingFunctions, function(item){
					loadingQs.push(item());
			});
			$q.all(loadingQs).then( success, fail);
		};
	};

}]);

angular.module('core').directive('loading', ['LoadingDirectiveService', function(LoadingDirectiveService){
	return{
		template: function(iElem, iAttr){
			return '<div ng-if="_loading" class="loading-spinner" id="loading-spinner-' + iAttr.key +  '"><div class="spinner-icon"></div></div>';
		},
		replace: true,
		scope:{
			key:'@',
		  default: '='
		},
		link: function(scope, element, attrs){
			scope._loading = angular.isDefined(scope.default) ? scope.default : true;
			scope.$on('event::loading::'+ attrs.key, function(event, obj){
					scope._loading = obj.done === true? false :true;
			});
		},
		controller: [
			'LoadingDirectiveService', '$q','$log', function(LoadingDirectiveService, $scope){
				$scope._loading = true;
			}
		]
	};


}]);

angular.module('core').service('LoadingDirectiveService', ['$timeout', '$rootScope',	'$q','$log', function($timeout, $rootScope, $q, $log){
	var isLoading = {};

	this.loaded = function(key){
		isLoading[key] = false;
		$rootScope.$broadcast('event::loading::'+ key, {done: true});
	};
	this.isLoading = function(key){
		var result = isLoading[key];
		return angular.isDefined(result)?result: false;
	};

	this.set = function(key, loading){
			isLoading[key] = loading;
			$rootScope.$broadcast('event::loading::'+ key, {done: !loading});
	};

	this.loading = function(key){
		isLoading[key] = true;
		$rootScope.$broadcast('event::loading::'+ key, {});
	};

}]);


angular.module('core').controller('HomeController', ['Espn', 'LoadingService', 'LoadingDirectiveService', 'Authentication', '$q', '$log', '$scope',
	function(Espn, LoadingService, LoadingDirectiveService, Authentication, $q, $log, $scope ) {
		// This provides Authentication context.
		var that =this;
		$scope.loading = true;

		$scope.authentication = Authentication;
		console.log(Authentication);
		this.loadingService = new LoadingService();
		$scope.data = {};

		var loadData = function(category){
			return function(){
				var defer = $q.defer();
				var func = Espn[category];

				if(func){
					func({limit: 50}).then(	function(data){
							$log.info(arguments);
							$scope.data[category] = data;
							$log.info('setting data', category, data);
							defer.resolve(data);
						}, function(data){
							defer.reject(data);
						});
				}else{
						defer.reject('failed to find func:' + category);
				}
				return defer.promise;
			};
		};

		this.loadingService.waitFor(loadData('top'));
		this.loadingService.waitFor(loadData('now'));
		this.loadingService.waitFor(loadData('popular'));


		function success(){
			$scope.loading = false;
		}

		function fail(){
			$scope.loading = false;
			$log.error('failed to load all modules', arguments);
		}
		$scope.initialize = function(){
			$scope.initializing = true;
			that.loadingService.initialize(success, fail);
		};
		$scope.isLoading = function(name){
				return LoadingDirectiveService.isLoading(name);
		};

		$scope.toggle = function(name){
			if(LoadingDirectiveService.isLoading(name)){
				LoadingDirectiveService.loaded(name);
			}else{
				LoadingDirectiveService.loading(name);

			}
		};
	}
]);
