(function(){
  'use strict';
  window.String.prototype.capitalize = function(){ return this.charAt(0).toUpperCase() + this.slice(1); };
  window.String.prototype.uncapitalize = function(){
      return (this.substring(0, 1) || '').toLowerCase() + (this.substring(1) || '');
  };
})();
