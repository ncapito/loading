'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName,
		ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.hashPrefix('!');
	}
]);
angular.module(ApplicationConfiguration.applicationModuleName).run(['editableOptions','editableThemes',
	function(editableOptions, editableThemes){
		editableOptions.theme = 'bs3';
		editableThemes.bs3.formTpl = '<form class="form" role="form"></form>';
	}
]);



angular.module(ApplicationConfiguration.applicationModuleName).constant('Keys',{
	ESPN: 'x3ffsa6frdw87gemwf4byuvh'
});


//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
