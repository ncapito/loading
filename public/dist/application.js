(function () {
  'use strict';
  window.String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
  };
  window.String.prototype.uncapitalize = function () {
    return (this.substring(0, 1) || '').toLowerCase() + (this.substring(1) || '');
  };
}());'use strict';
// Init the application configuration module for AngularJS application
var ApplicationConfiguration = function () {
    // Init module configuration options
    var applicationModuleName = 'mean';
    var applicationModuleVendorDependencies = [
        'ngResource',
        'ngAnimate',
        'ui.router',
        'ui.bootstrap',
        'ui.utils',
        'angular-loading-bar',
        'angular-spinkit',
        'xeditable'
      ];
    // Add a new vertical module
    var registerModule = function (moduleName) {
      // Create angular module
      angular.module(moduleName, []);
      // Add the module to the AngularJS configuration file
      angular.module(applicationModuleName).requires.push(moduleName);
    };
    return {
      applicationModuleName: applicationModuleName,
      applicationModuleVendorDependencies: applicationModuleVendorDependencies,
      registerModule: registerModule
    };
  }();'use strict';
//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);
// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config([
  '$locationProvider',
  function ($locationProvider) {
    $locationProvider.hashPrefix('!');
  }
]);
angular.module(ApplicationConfiguration.applicationModuleName).run([
  'editableOptions',
  'editableThemes',
  function (editableOptions, editableThemes) {
    editableOptions.theme = 'bs3';
    editableThemes.bs3.formTpl = '<form class="form" role="form"></form>';
  }
]);
angular.module(ApplicationConfiguration.applicationModuleName).constant('Keys', { ESPN: 'x3ffsa6frdw87gemwf4byuvh' });
//Then define the init function for starting up the application
angular.element(document).ready(function () {
  //Fixing facebook bug with redirect
  if (window.location.hash === '#_=_')
    window.location.hash = '#!';
  //Then init the app
  angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('admin');'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('espn');'use strict';
//Articles service used for communicating with the articles REST endpoints
angular.module('espn').factory('Espn', [
  'Keys',
  '$q',
  '$http',
  function (Keys, $q, $http) {
    var coreData = {
        apikey: Keys.ESPN,
        JSON_CALLBACK: ''
      };
    function handleError(response) {
      // The API response from the server should be returned in a
      // nomralized format. However, if the request was not handled by the
      // server (or what not handles properly - ex. server error), then we
      // may have to normalize it on our end, as best we can.
      if (!angular.isObject(response.data) || !response.data.message) {
        return $q.reject('An unknown error occurred.');
      }
      // Otherwise, use expected error message.
      return $q.reject(response.data.message);
    }
    // I transform the successful response, unwrapping the application data
    // from the API response payload.
    function handleSuccess(response) {
      return response.data;
    }
    return {
      top: function (data) {
        var request = $http.get('http://api.espn.com/v1/now/top', { params: angular.extend({}, coreData, data) });
        return request.then(handleSuccess, handleError);
      },
      now: function (data) {
        var request = $http.get('http://api.espn.com/v1/now', { params: angular.extend({}, coreData, data) });
        return request.then(handleSuccess, handleError);
      },
      popular: function (data) {
        var request = $http.get('http://api.espn.com/v1/now/popular', { params: angular.extend({}, coreData, data) });
        return request.then(handleSuccess, handleError);
      }
    };
  }
]);'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('product');'use strict';
//Articles service used for communicating with the articles REST endpoints
angular.module('product').factory('Product', [
  '$resource',
  function ($resource) {
    return $resource('/products/:productID', { productID: '@_id' }, { update: { method: 'PUT' } });
  }
]).factory('Persona', [
  '$resource',
  function ($resource) {
    return $resource('/Persona/:personaID', { personaID: '@_id' }, { update: { method: 'PUT' } });
  }
]);'use strict';
angular.module('product').directive('contenteditable', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, ngModel) {
      function read() {
        if (ngModel.$viewValue !== element.html()) {
          ngModel.$setViewValue(element.html());
        }
      }
      ngModel.$render = function () {
        element.html(ngModel.$viewValue || '');
      };
      element.bind('blur keyup change', function () {
        scope.$apply(read);
      });
    }
  };
});
angular.module('product').controller('ProductCtrl', [
  'Product',
  '$scope',
  '$state',
  '$location',
  'Authentication',
  function (Product, $scope, $state, $location, Authentication) {
    $scope.authentication = Authentication;
  }
]).controller('ProductListCtrl', [
  'Product',
  '$scope',
  '$state',
  '$location',
  'Authentication',
  function (Product, $scope, $state, $location, Authentication) {
    Product.query(function (data) {
      $scope.products = data;
    }, function (error) {
      alert('error');
    });
  }
]).controller('ProductShowCtrl', [
  'Product',
  '$log',
  '$scope',
  '$state',
  '$location',
  'Authentication',
  function (Product, $log, $scope, $state, $location, Authentication) {
    $scope.$state = $state;
    $scope.loading = false;
    $scope.save = save;
    $scope.isEditing = isEditing;
    $scope.cancel = cancel;
    $scope.edit = edit;
    $scope.prevent = prevent;
    $scope.validate = validate;
    $scope.saveField = saveField;
    var _edit = {};
    function saveField(eventType, data) {
      if (validate(eventType, data) === true) {
        save();
      }
    }
    function validate(eventType, data) {
      $log.info($scope.product, data);
      if (angular.isDefined(data) && data) {
        return true;
      }
      return eventType + ' is required';
    }
    function prevent($event) {
      $event.preventDefault();
      $event.stopPropagation();
    }
    function isEditing(key) {
      return _edit[key];
    }
    function cancel(form) {
      if (form) {
        form.$setPristine();
      }
      _edit = {};
      $scope.product.$get();
    }
    function edit(key) {
      _edit[key] = true;
    }
    function save(form) {
      var name = '$update';
      if (!$scope.product._id) {
        name = '$save';
      }
      $scope.product[name](success, error);
      function success(result) {
        if (form) {
          form.$setPristine();
        }
        $scope.loading = false;
        _edit = {};
        $state.go('product.show', { id: $scope.product._id });
      }
      function error(result) {
        $log.error(result);
        $scope.loading = false;
      }
    }
    $log.info('loading product ', $state.params.id);
    $scope.product = new Product({ _id: $state.params.id || undefined });
    if ($scope.product._id) {
      $scope.product.$get();
    }
  }
]);'use strict';
// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users');'use strict';
// Configuring the Articles module
angular.module('admin').run([
  'Menus',
  function (Menus) {
  }
]);'use strict';
// Setting up route
angular.module('admin').config([
  '$stateProvider',
  function ($stateProvider) {
    // Peoples state routing
    function scaffold(resource) {
      var lowerResource = resource.toLowerCase(), capitalizedResource = resource.capitalize(), firstLetterLower = resource.uncapitalize();
      $stateProvider.state('admin.' + lowerResource, {
        abstract: true,
        url: '/' + lowerResource + '/',
        template: '<div ui-view> </div>',
        controller: capitalizedResource + 'Ctrl'
      }).state('admin.' + lowerResource + '.list', {
        url: '',
        templateUrl: 'modules/admin/' + lowerResource + '/views/list.html',
        controller: capitalizedResource + 'ListCtrl'
      }).state('admin.' + lowerResource + '.create', {
        url: 'create',
        templateUrl: 'modules/admin/' + lowerResource + '/views/create.html',
        controller: capitalizedResource + 'CreateCtrl'
      }).state('admin.' + lowerResource + '._', {
        url: ':' + firstLetterLower + 'Id',
        abstract: true,
        template: '<div ui-view> </div>',
        controller: capitalizedResource + 'ShowCtrl'
      }).state('admin.' + lowerResource + '._.view', {
        url: '',
        templateUrl: 'modules/admin/' + lowerResource + '/views/show.html'
      }).state('admin.' + lowerResource + '._.edit', {
        url: '/edit',
        templateUrl: 'modules/admin/' + resource + '/views/edit.html',
        controller: capitalizedResource + 'EditCtrl'
      });
    }
    $stateProvider.state('admin', {
      url: '/admin',
      abstract: true,
      template: '<div ui-view> </div>'
    });
    scaffold('people');
    scaffold('project');
    scaffold('gameMeta');
  }
]);'use strict';
angular.module('admin').controller('ProjectListCtrl', [function () {
  }]);
angular.module('admin').controller('ProjectShowCtrl', [function () {
  }]);
angular.module('admin').controller('ProjectCreateCtrl', [function () {
  }]);
angular.module('admin').controller('ProjectEditCtrl', [function () {
  }]);
angular.module('admin').controller('ProjectCtrl', [
  '$scope',
  '$stateParams',
  '$state',
  '$location',
  'Authentication',
  'Project',
  function ($scope, $stateParams, $state, $location, Authentication, Project) {
    $scope.authentication = Authentication;
    $scope.create = function () {
      var project = new Project({
          title: this.title,
          content: this.content
        });
      project.$save(function (response) {
        $state.go('admin.project._.view', { projectId: response._id });
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
      this.title = '';
      this.content = '';
    };
    $scope.remove = function (project) {
      if (project) {
        project.$remove();
        for (var i in $scope.projects) {
          if ($scope.projects[i] === project) {
            $scope.projects.splice(i, 1);
          }
        }
      } else {
        $scope.project.$remove(function () {
          $state.go('admin.project.list');
        });
      }
    };
    $scope.update = function () {
      var project = $scope.project;
      project.$update(function (response) {
        $state.go('admin.project._.view', { projectId: response._id });
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    $scope.find = function () {
      $scope.projects = Project.query();
    };
    $scope.findOne = function () {
      $scope.project = Project.get({ projectId: $stateParams.projectId });
    };
  }
]);'use strict';
//Articles service used for communicating with the articles REST endpoints
angular.module('admin').factory('Project', [
  '$resource',
  function ($resource) {
    return $resource('admin/project/:projectId', { peopleId: '@_id' }, { update: { method: 'PUT' } });
  }
]);'use strict';
// Setting up route
angular.module('core').config([
  '$stateProvider',
  '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {
    // Redirect to home view when route not found
    $urlRouterProvider.otherwise('/');
    // Home state routing
    $stateProvider.state('home', {
      url: '/',
      templateUrl: 'modules/core/views/home.client.view.html'
    });
  }
]);'use strict';
angular.module('core').controller('HeaderController', [
  '$scope',
  'Authentication',
  'Menus',
  function ($scope, Authentication, Menus) {
    $scope.authentication = Authentication;
    $scope.isCollapsed = false;
    $scope.menu = Menus.getMenu('topbar');
    $scope.toggleCollapsibleMenu = function () {
      $scope.isCollapsed = !$scope.isCollapsed;
    };
    // Collapsing the menu after navigation
    $scope.$on('$stateChangeSuccess', function () {
      $scope.isCollapsed = false;
    });
  }
]);'use strict';
angular.module('core').factory('LoadingService', [
  '$q',
  function ($q) {
    return function () {
      var loadingFunctions = [];
      var loadingQs = [];
      this.waitFor = function (func) {
        loadingFunctions.push(func);
      };
      this.initialize = function (success, fail) {
        angular.forEach(loadingFunctions, function (item) {
          loadingQs.push(item());
        });
        $q.all(loadingQs).then(success, fail);
      };
    };
  }
]);
angular.module('core').directive('loading', [
  'LoadingDirectiveService',
  function (LoadingDirectiveService) {
    return {
      template: function (iElem, iAttr) {
        return '<div ng-if="_loading" class="loading-spinner" id="loading-spinner-' + iAttr.key + '"><div class="spinner-icon"></div></div>';
      },
      replace: true,
      scope: {
        key: '@',
        default: '='
      },
      link: function (scope, element, attrs) {
        scope._loading = angular.isDefined(scope.default) ? scope.default : true;
        scope.$on('event::loading::' + attrs.key, function (event, obj) {
          scope._loading = obj.done === true ? false : true;
        });
      },
      controller: [
        'LoadingDirectiveService',
        '$q',
        '$log',
        function (LoadingDirectiveService, $scope) {
          $scope._loading = true;
        }
      ]
    };
  }
]);
angular.module('core').service('LoadingDirectiveService', [
  '$timeout',
  '$rootScope',
  '$q',
  '$log',
  function ($timeout, $rootScope, $q, $log) {
    var isLoading = {};
    this.loaded = function (key) {
      isLoading[key] = false;
      $rootScope.$broadcast('event::loading::' + key, { done: true });
    };
    this.isLoading = function (key) {
      var result = isLoading[key];
      return angular.isDefined(result) ? result : false;
    };
    this.set = function (key, loading) {
      isLoading[key] = loading;
      $rootScope.$broadcast('event::loading::' + key, { done: !loading });
    };
    this.loading = function (key) {
      isLoading[key] = true;
      $rootScope.$broadcast('event::loading::' + key, {});
    };
  }
]);
angular.module('core').controller('HomeController', [
  'Espn',
  'LoadingService',
  'LoadingDirectiveService',
  'Authentication',
  '$q',
  '$log',
  '$scope',
  function (Espn, LoadingService, LoadingDirectiveService, Authentication, $q, $log, $scope) {
    // This provides Authentication context.
    var that = this;
    $scope.loading = true;
    $scope.authentication = Authentication;
    console.log(Authentication);
    this.loadingService = new LoadingService();
    $scope.data = {};
    var loadData = function (category) {
      return function () {
        var defer = $q.defer();
        var func = Espn[category];
        if (func) {
          func({ limit: 50 }).then(function (data) {
            $log.info(arguments);
            $scope.data[category] = data;
            $log.info('setting data', category, data);
            defer.resolve(data);
          }, function (data) {
            defer.reject(data);
          });
        } else {
          defer.reject('failed to find func:' + category);
        }
        return defer.promise;
      };
    };
    this.loadingService.waitFor(loadData('top'));
    this.loadingService.waitFor(loadData('now'));
    this.loadingService.waitFor(loadData('popular'));
    function success() {
      $scope.loading = false;
    }
    function fail() {
      $scope.loading = false;
      $log.error('failed to load all modules', arguments);
    }
    $scope.initialize = function () {
      $scope.initializing = true;
      that.loadingService.initialize(success, fail);
    };
    $scope.isLoading = function (name) {
      return LoadingDirectiveService.isLoading(name);
    };
    $scope.toggle = function (name) {
      if (LoadingDirectiveService.isLoading(name)) {
        LoadingDirectiveService.loaded(name);
      } else {
        LoadingDirectiveService.loading(name);
      }
    };
  }
]);'use strict';
//Menu service used for managing  menus
angular.module('core').service('Menus', [function () {
    // Define a set of default roles
    this.defaultRoles = ['user'];
    // Define the menus object
    this.menus = {};
    // A private function for rendering decision
    var shouldRender = function (user) {
      if (user) {
        for (var userRoleIndex in user.roles) {
          for (var roleIndex in this.roles) {
            if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
              return true;
            }
          }
        }
      } else {
        return this.isPublic;
      }
      return false;
    };
    // Validate menu existance
    this.validateMenuExistance = function (menuId) {
      if (menuId && menuId.length) {
        if (this.menus[menuId]) {
          return true;
        } else {
          throw new Error('Menu does not exists');
        }
      } else {
        throw new Error('MenuId was not provided');
      }
      return false;
    };
    // Get the menu object by menu id
    this.getMenu = function (menuId) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Return the menu object
      return this.menus[menuId];
    };
    // Add new menu object by menu id
    this.addMenu = function (menuId, isPublic, roles) {
      // Create the new menu
      this.menus[menuId] = {
        isPublic: isPublic || false,
        roles: roles || this.defaultRoles,
        items: [],
        shouldRender: shouldRender
      };
      // Return the menu object
      return this.menus[menuId];
    };
    // Remove existing menu object by menu id
    this.removeMenu = function (menuId) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Return the menu object
      delete this.menus[menuId];
    };
    // Add menu item object
    this.addMenuItem = function (options) {
      // Validate that the menu exists
      //id, title, url, type, route, isPublic, roles
      this.validateMenuExistance(options.id);
      // Push new menu item
      this.menus[options.id].items.push({
        title: options.title,
        link: options.url,
        category: options.category,
        menuItemType: options.type || 'item',
        menuItemClass: options.type,
        route: options.route || '/' + options.url,
        isPublic: options.isPublic || this.menus[options.id].isPublic,
        roles: options.roles || this.defaultRoles,
        items: [],
        shouldRender: shouldRender
      });
      // Return the menu object
      return this.menus[options.id];
    };
    // Add submenu item object
    this.addSubMenuItem = function (options) {
      /*
			var options = {
				parentId: menuId,
				link: rootMenuItemURL,
				title: menuItemTitle,
				route: menuItemUIRoute,
				isPublic: isPublic,
				roles: roles
			};*/
      // Validate that the menu exists
      this.validateMenuExistance(options.parentId);
      // Search for menu item
      for (var itemIndex in this.menus[options.parentId].items) {
        // Push new submenu item
        if (this.menus[options.parentId].items[itemIndex].category !== options.category) {
          continue;
        }
        this.menus[options.parentId].items[itemIndex].items.push({
          title: options.title,
          link: options.link,
          category: options.category,
          route: options.route || '/' + options.link,
          isPublic: options.isPublic || this.menus[options.parentId].isPublic,
          roles: options.roles || this.defaultRoles,
          shouldRender: shouldRender
        });
      }
      // Return the menu object
      return this.menus[options.parentId];
    };
    // Remove existing menu object by menu id
    this.removeMenuItem = function (menuId, menuItemURL) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Search for menu item to remove
      for (var itemIndex in this.menus[menuId].items) {
        if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
          this.menus[menuId].items.splice(itemIndex, 1);
        }
      }
      // Return the menu object
      return this.menus[menuId];
    };
    // Remove existing menu object by menu id
    this.removeSubMenuItem = function (menuId, submenuItemURL) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);
      // Search for menu item to remove
      for (var itemIndex in this.menus[menuId].items) {
        for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
          if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
            this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
          }
        }
      }
      // Return the menu object
      return this.menus[menuId];
    };
    //Adding the topbar menu
    this.addMenu('topbar');
  }]);'use strict';
angular.module('espn').run([
  'Menus',
  function (Menus) {
  }
]);'use strict';
// Setting up route
angular.module('espn').config([
  '$stateProvider',
  function ($stateProvider) {
  }
]);'use strict';
// Configuring the Articles module
angular.module('product').run([
  'Menus',
  function (Menus) {
    // Set top bar menu items
    Menus.addMenuItem({
      id: 'topbar',
      category: 'products',
      title: 'Products',
      type: 'dropdown'
    });
    Menus.addSubMenuItem({
      parentId: 'topbar',
      category: 'products',
      title: 'List Products',
      route: 'product.list'
    });
  }
]);'use strict';
// Setting up route
angular.module('product').config([
  '$stateProvider',
  function ($stateProvider) {
    // Peoples state routing
    $stateProvider.state('product', {
      url: '/product',
      abstract: true,
      template: '<div ui-view> </div>',
      controller: 'ProductCtrl'
    }).state('product.list', {
      url: '',
      templateUrl: 'modules/product/product.list.html',
      controller: 'ProductListCtrl'
    }).state('product.show', {
      url: '/:id',
      templateUrl: 'modules/product/product.show.html',
      controller: 'ProductShowCtrl'
    }).state('product.show.newpersona', {
      url: '/persona',
      templateUrl: 'modules/product/persona/persona.new.html',
      controller: 'PersonaShowCtrl'
    }).state('product.show.persona', {
      url: '/persona/:personaID',
      templateUrl: 'modules/product/persona/persona.show.html',
      controller: 'PersonaShowCtrl'
    });
  }
]);'use strict';
angular.module('product').controller('PersonaShowCtrl', [
  'Persona',
  '$log',
  '$scope',
  '$state',
  '$location',
  'Authentication',
  function (Persona, $log, $scope, $state, $location, Authentication) {
    $scope.loading = false;
    $scope.save = save;
    $scope.isEditing = isEditing;
    $scope.cancel = cancel;
    $scope.edit = edit;
    $scope.prevent = prevent;
    $scope.validate = validate;
    $scope.saveField = saveField;
    var _edit = {};
    function saveField(eventType, data) {
      if (validate(eventType, data) === true) {
        save();
      }
    }
    function validate(eventType, data) {
      $log.info($scope.product, data);
      if (angular.isDefined(data) && data) {
        return true;
      }
      return eventType + ' is required';
    }
    function prevent($event) {
      $event.preventDefault();
      $event.stopPropagation();
    }
    function isEditing(key) {
      return _edit[key];
    }
    function cancel(form) {
      if (form) {
        form.$setPristine();
      }
      _edit = {};
      $scope.product.$get();
    }
    function edit(key) {
      _edit[key] = true;
    }
    function save(form) {
      var name = '$update';
      if (!$scope.product._id) {
        name = '$save';
      }
      $scope.product[name](success, error);
      function success(result) {
        if (form) {
          form.$setPristine();
        }
        $scope.loading = false;
        _edit = {};
        $state.go('product.show', { id: $scope.product._id });
      }
      function error(result) {
        $log.error(result);
        $scope.loading = false;
      }
    }
    $log.info('loading Persona ', $state.params.id);
    $scope.product = new Persona({
      _id: $state.params.personaID || undefined,
      goals: [],
      frustrations: [],
      behavoirs: []
    });
    if ($scope.product._id) {
      $scope.product.$get();
    }
  }
]);'use strict';
// Config HTTP Error Handling
angular.module('users').config([
  '$httpProvider',
  function ($httpProvider) {
    // Set the httpProvider "not authorized" interceptor
    $httpProvider.interceptors.push([
      '$q',
      '$location',
      'Authentication',
      function ($q, $location, Authentication) {
        return {
          responseError: function (rejection) {
            switch (rejection.status) {
            case 401:
              // Deauthenticate the global user
              Authentication.user = null;
              // Redirect to signin page
              $location.path('signin');
              break;
            case 403:
              // Add unauthorized behaviour 
              break;
            }
            return $q.reject(rejection);
          }
        };
      }
    ]);
  }
]);'use strict';
// Setting up route
angular.module('users').config([
  '$stateProvider',
  function ($stateProvider) {
    // Users state routing
    $stateProvider.state('profile', {
      url: '/settings/profile',
      templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
    }).state('password', {
      url: '/settings/password',
      templateUrl: 'modules/users/views/settings/change-password.client.view.html'
    }).state('accounts', {
      url: '/settings/accounts',
      templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
    }).state('signup', {
      url: '/signup',
      templateUrl: 'modules/users/views/signup.client.view.html'
    }).state('signin', {
      url: '/signin',
      templateUrl: 'modules/users/views/signin.client.view.html'
    });
  }
]);'use strict';
angular.module('users').controller('AuthenticationController', [
  '$scope',
  '$http',
  '$location',
  'Authentication',
  function ($scope, $http, $location, Authentication) {
    $scope.authentication = Authentication;
    //If user is signed in then redirect back home
    if ($scope.authentication.user)
      $location.path('/');
    $scope.signup = function () {
      $http.post('/auth/signup', $scope.credentials).success(function (response) {
        //If successful we assign the response to the global user model
        $scope.authentication.user = response;
        //And redirect to the index page
        $location.path('/');
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
    $scope.signin = function () {
      $http.post('/auth/signin', $scope.credentials).success(function (response) {
        //If successful we assign the response to the global user model
        $scope.authentication.user = response;
        //And redirect to the index page
        $location.path('/');
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);'use strict';
angular.module('users').controller('SettingsController', [
  '$scope',
  '$http',
  '$location',
  'Users',
  'Authentication',
  function ($scope, $http, $location, Users, Authentication) {
    $scope.user = Authentication.user;
    // If user is not signed in then redirect back home
    if (!$scope.user)
      $location.path('/');
    // Check if there are additional accounts
    $scope.hasConnectedAdditionalSocialAccounts = function (provider) {
      for (var i in $scope.user.additionalProvidersData) {
        return true;
      }
      return false;
    };
    // Check if provider is already in use with current user
    $scope.isConnectedSocialAccount = function (provider) {
      return $scope.user.provider === provider || $scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider];
    };
    // Remove a user social account
    $scope.removeUserSocialAccount = function (provider) {
      $scope.success = $scope.error = null;
      $http.delete('/users/accounts', { params: { provider: provider } }).success(function (response) {
        // If successful show success message and clear form
        $scope.success = true;
        $scope.user = Authentication.user = response;
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
    // Update a user profile
    $scope.updateUserProfile = function () {
      $scope.success = $scope.error = null;
      var user = new Users($scope.user);
      user.$update(function (response) {
        $scope.success = true;
        Authentication.user = response;
      }, function (response) {
        $scope.error = response.data.message;
      });
    };
    $scope.addRole = function (name) {
      if (!~$scope.user.roles.indexOf(name)) {
        $scope.user.roles.push(name);
        $scope.tempRole = '';
      }
    };
    // Change user password
    $scope.changeUserPassword = function () {
      $scope.success = $scope.error = null;
      $http.post('/users/password', $scope.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.success = true;
        $scope.passwordDetails = null;
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);'use strict';
// Authentication service for user variables
angular.module('users').factory('Authentication', [function () {
    var _this = this;
    _this._data = { user: window.user };
    return _this._data;
  }]);'use strict';
// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', [
  '$resource',
  function ($resource) {
    return $resource('users', {}, { update: { method: 'PUT' } });
  }
]);