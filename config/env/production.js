'use strict';

function getHost(){
	return process.env.HOST || 'http://localhost:3000';
}

module.exports = {
	db: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://localhost/mean',
	host: getHost() ,
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.min.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.min.css',
				'public/lib/font-awesome/css/font-awesome.min.css',
				'public/lib/angular-loading-bar/build/loading-bar.min.css',
				'public/lib/angular-spinkit/build/angular-spinkit.min.css',
				'public/lib/angular-xeditable/dist/css/xeditable.css'
			],
			js: [
				'public/lib/jquery/dist/jquery.js',
				'public/lib/angular/angular.js',
				'public/lib/angular-xeditable/dist/js/xeditable.min.js',
				'public/lib/angular-resource/angular-resource.min.js',
				'public/lib/angular-animate/angular-animate.min.js',
				'public/lib/angular-ui-router/release/angular-ui-router.min.js',
				'public/lib/angular-ui-utils/ui-utils.min.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
				'public/lib/angular-loading-bar/build/loading-bar.min.js',
				'public/lib/angular-spinkit/build/angular-spinkit.min.js',
				'public/lib/angular-timer/dist/angular-timer.min.js'
			]
		},
		css: [	'public/modules/**/css/*.css'],
		js: [		'public/utilities.js',
		'public/config.js',
		'public/application.js',
		'public/modules/*/*.js',
		'public/modules/ext/AngularJS-maphilights/*.js',
		'public/modules/*/*[!tests]*/*.js']
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || 'APP_ID',
		clientSecret: process.env.FACEBOOK_SECRET || 'APP_SECRET',
		callbackURL: getHost() + '/auth/facebook/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
		clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
		callbackURL: getHost() + '/auth/twitter/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || 'APP_ID',
		clientSecret: process.env.GOOGLE_SECRET || 'APP_SECRET',
		callbackURL: getHost() + '/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || 'APP_ID',
		clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
		callbackURL: getHost() + '/auth/linkedin/callback'
	}
};
