'use strict';

module.exports = {
	app: {
		title: 'Unboxed PM',
		description: 'Full-Stack JavaScript with MongoDB, Express, AngularJS, and Node.js',
		keywords: 'mongodb, express, angularjs, node.js, mongoose, passport'
	},
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
	sessionSecret: 'MEAN',
	sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.css',
				'public/lib/font-awesome/css/font-awesome.css',
				'public/lib/angular-loading-bar/build/loading-bar.css',
				'public/lib/angular-spinkit/build/angular-spinkit.min.css',
				'public/lib/angular-xeditable/dist/css/xeditable.css'

			],
			js: [
				'public/lib/jquery/dist/jquery.js',
				'public/lib/angular/angular.js',
				'public/lib/angular-xeditable/dist/js/xeditable.js',
				'public/lib/angular-resource/angular-resource.js',
				'public/lib/angular-animate/angular-animate.js',
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
				'public/lib/angular-loading-bar/build/loading-bar.js',
				'public/lib/angular-spinkit/build/angular-spinkit.js',
				'public/lib/angular-timer/dist/angular-timer.js'
			]
		},
		css: [
			'public/modules/**/css/*.css'
		],
		js: [
			'public/utilities.js',
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/ext/AngularJS-maphilights/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};
