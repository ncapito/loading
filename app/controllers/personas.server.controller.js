'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Persona = mongoose.model('Persona'),
	util = require('./controller.server.util'),
	_ = require('lodash');


/**
 * Show the current
 */
exports.read = read;

function read(req, res) {
	res.jsonp(req.model);
}

exports.delete = function(req, res) {
	var model = req.model;

	model.remove(function(err) {
		if (err) {
			return res.send(400, {
				message: util.getErrorMessage(err)
			});
		} else {
			res.jsonp(model);
		}
	});
};


/**
 * List of Articles
 */
exports.list = function(req, res) {
	var obj = {};

	Persona.find(req.query).sort('-created').exec(function(err, products) {
		if (err) {
			return res.send(400, {
				message: util.getErrorMessage(err)
			});
		} else {
			res.jsonp(products);
		}
	});
};

function ensureProductHasPersona(persona, req){
	console.log('getting product', persona.product);
	var promise = new mongoose.Promise();

	persona.getProduct().then(function(product){
		console.log('done product');

		//make sure product is in the list of personas.. if not add it.
		if(!product.getPersona(persona)){
			product.personas.push(persona);
			product.save(req, function(err){
				if(err){
					promise.error(err);
				}
				promise.complete(persona);
			});
		}else{
			//persona already exists...
			console.log('persona exists returning');
			promise.complete(persona);
		}
	});

	return promise;
}


exports.create = function(req, res) {
	var persona = new Persona(req.body);
	console.log('saving persona');
	persona.save(req, function(error, model, numberSaved){
		console.log('Inside of save');
		if(error){
			console.log('error saving persona');
			return util.handleError(error, res);
		}
		success();

	});

	function success(){
		//if not lets save the persona in the product
		console.log('ensuring product has persona...', persona);

		ensureProductHasPersona(persona, req).then(_success,error );

		function error(err){
			console.error('failed to ensure persona', err);
			return util.handleError(err, res);
		}


		function _success(){
			console.log('returning ', persona._id);
			return getByID(req,res, persona._id);
		}

	}

};


/**
* Update a people
*/
exports.update = function(req, res) {
	var persona = req.model;

	persona = _.extend(persona, req.body);

	console.log('updating persona', persona);
	persona.save(req, function(err) {
		if (err) {
			util.handleError(err, req);
		} else {
				return getByID(req,res, persona._id);
		}
	});
};


function getByID(req, res, id) {
	return byID(req,res, function(){ read(req, res); }, id);
}
/**
 * People middleware
 */
function byID(req, res, next, id) {
	Persona.findById(id).populate('product').exec(function(err, model) {
		if (err) return next(err);
		if (!model) return next(new Error('Failed to load persona ' + id));
		console.log(model);
		req.model = model;
		if(next){
			next();
		}
	});
}

exports.byID =byID;
