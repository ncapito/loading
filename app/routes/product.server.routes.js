'use strict';


var products = require('../../app/controllers/products.server.controller'),
		personas = require('../../app/controllers/personas'),
								users = require('../../app/controllers/users');

module.exports = function(app) {
	// Article Routes
	app.route('/products')
		.get(products.list)
		.post(users.requiresLogin, products.create);

	app.route('/products/:productID')
		.get(products.read)
		.put(users.requiresLogin, products.update)
		.delete(users.requiresLogin, products.delete);

	app.param('productID', products.byID);

	/**
	Personas
	**/
	app.route('/personas')
	.get(personas.list)
	.post(users.requiresLogin, personas.create);

	app.route('/personas/:personaID')
	.get(personas.read)
	.put(users.requiresLogin, personas.update)
	.delete(users.requiresLogin, personas.delete);

	app.param('personaID', personas.byID);



};
