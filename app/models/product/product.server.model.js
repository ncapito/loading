'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
		_ = require('lodash'),
		util = require('../model.util');


var ProductSchema = new Schema({
	createDate: {
		type: Date,
		default: Date.now
	},
	updateDate: {
		type: Date,
		default: Date.now
	},
	createdBy: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	updatedBy: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	name: {
		type: String,
		default: '',
		trim: true,
		required: 'Name cannot be blank'
	},
	vision: {
		type: 'String',
		default: '',
		trim: true
	},
	personas: [
	{
		type: Schema.ObjectId,
		ref: 'Persona'
	}
	],
	enhancements: [ 'OrderedObjectSchema'],
	features: [ 'OrderedObjectSchema'],
	problems: ['ProblemSchema'],
	roi: {}

});


ProductSchema.post('init', function(doc) {
	this.checkDefaults();
});

ProductSchema.pre('save', function(next, req, callback) {
	//figure out how to do this better
	this.checkDefaults();
	util.auditModel(this, req);
	util.checkIds(this.problems);
	util.checkIds(this.enhancements);
	util.checkIds(this.features);
	next(callback);
});


ProductSchema.methods.getPersona = function(model){
	var id = model && model._id || model;
	return _.findWhere(this.personas, {_id: id});
};

ProductSchema.methods.checkDefaults = function(){
	this.problems = this.problems || [];
	this.enhancements = this.enhancements || [];
	this.features = this.features || [];
};



var ProblemSchema = new Schema({
	description: {
		type: String,
		default: '',
		trim: true,
		required: 'Name cannot be blank'
	}
});

mongoose.model('Problem', ProblemSchema);
mongoose.model('Product', ProductSchema);

exports.Product = ProductSchema;
exports.Problem = ProblemSchema;
