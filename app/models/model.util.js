(function(exports, require) {

  'use strict';

  var _ = require('lodash');

  function checkIds(collection) {
    collection = collection || [];

    _.forEach(collection, function(item) {
      if (item && !item.id) {
        item.id = new Date().getTime();
      }
    });
  }

  function auditModel(model, req) {
    console.log('auditing model');
    var user = req && req.user && req.user.id;
    if (!model.createdBy) {
      model.createdBy = user;
    }

    model.updatedBy = user;
    model.updateDate = Date.now;
    console.log('done auditing model', model.updateDate);
  }
  exports.checkIds = checkIds;
  exports.auditModel = auditModel;
})(exports, require);
